ARG FEDORA_IMAGE_RT=fedora-minimal
ARG FEDORA_RELEASE=31
ARG HTTP_PROXY=""

FROM $FEDORA_IMAGE_RT:$FEDORA_RELEASE

ENV http_proxy=$HTTP_PROXY

WORKDIR /
VOLUME /etc/dockerregistry
VOLUME /var/log/dockerregistry
VOLUME /var/lib/dockerregistry

ADD ./rpmreqs-rt.txt ./

RUN microdnf -y update \
    && microdnf -y install $(cat rpmreqs-rt.txt) \
    && microdnf -y clean all \
    && rm -fv rpmreqs-rt.txt


RUN adduser -u 1010 -r -g root -d /var/lib/dockerregistry -s /sbin/nologin -c "registry user" registry \
    && chown -R registry.root /var/lib/dockerregistry
ENV USER=registry
ENV CHOWN=true
ENV CHOWN_DIRS="/var/lib/dockerregistry/,/var/log/dockerregistry/"

ADD ./entrypoint.sh /sbin/
ADD ./install.sh /sbin/
ADD ./uninstall.sh /sbin/
RUN chmod +x /sbin/entrypoint.sh
RUN chmod +x /sbin/install.sh
RUN chmod +x /sbin/uninstall.sh

ADD ./config.yml.default  /root/
ENV REGISTRY_OPENSHIFT_SERVER_ADDR=127.0.0.1
#ENV REGISTRY_HTTP_SECRET
EXPOSE 5000

ENTRYPOINT ["/sbin/entrypoint"]
CMD ["/usr/bin/dockerregistry","--log_dir /var/log/dockerregistry","/etc/dockerregistry/config.yml"]

LABEL INSTALL="podman run --rm -t -i --privileged --rm --net=host --ipc=host --pid=host -v /:/host -v /run:/run -e HOST=/host -e IMAGE=\$IMAGE -e NAME=\$NAME -e CONFDIR=/etc -e LOGDIR=/var/log -e DATADIR=/var/lib --entrypoint /bin/sh  \$IMAGE /sbin/install.sh"
LABEL UNINSTALL="podman run --rm -t -i --privileged --rm --net=host --ipc=host --pid=host -v /:/host -v /run:/run -e HOST=/host -e IMAGE=\$IMAGE -e NAME=\$NAME -e CONFDIR=/etc -e LOGDIR=/var/log -e DATADIR=/var/lib --entrypoint /bin/sh  \$IMAGE /sbin/uninstall.sh"

