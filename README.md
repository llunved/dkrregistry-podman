# dkrregistry-podman

Podman-built container for Docker Registry built with Fedora and Origin Registry RPMs.
BUILD:

sudo podman build --build-arg FEDORA_RELEASE=31 --build-arg FEDORA_IMAGE_RT=llunved/fedora-minimal -t llunved/dockerregistry:$(date +%s) -f Dockerfile

