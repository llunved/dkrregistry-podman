#!/bin/bash


set -x

SERVICE=$IMAGE

env

# Make sure the host is mounted
if [ ! -d /host/etc -o ! -d /host/proc -o ! -d /host/var/run ]; then
	    echo "cockpit-run: host file system is not mounted at /host" >&2
	        exit 1
fi

# Make sure that we have required directories in the host
for CUR_DIR in /host/$CONFDIR /host/$LOGDIR /host/$DATADIR; do
    CUR_DIR=${CUR_DIR}/${NAME}
    if [ -n $CUR_DIR ]; then
        mkdir -p $CUR_DIR
        chmod -R 775 $CUR_DIR
	chgrp 0 $CUR_DIR
    fi
done    

# Install default config if no config founf
if [ -n /host/${CONFDIR}/${NAME}/config.yml ]; then
    cat /root/config.yml.default > /host/${CONFDIR}/${NAME}/config.yml
else
    cat /root/config.yml.default > /host/${CONFDIR}/${NAME}/config.yml.new
fi

REGISTRY_OPENSHIFT_SERVER_ADDR=`chroot /host hostname -f`

chroot /host /usr/bin/podman create --name ${NAME} -p 5000:5000 --entrypoint /sbin/entrypoint.sh -v ${CONFDIR}/${NAME}:/etc/dockerregistry/ -v ${DATADIR}/${NAME}:/var/lib/dockerregistry/ -v ${LOGDIR}/${NAME}:/var/log/dockerregistry/ -e REGISTRY_OPENSHIFT_SERVER_ADDR=${REGISTRY_OPENSHIFT_SERVER_ADDR} ${IMAGE} /usr/bin/dockerregistry --log_dir /var/log/dockerregistry /etc/dockerregistry/config.yml 
chroot /host sh -c "/usr/bin/podman generate systemd --restart-policy=always -t 1 ${NAME} > /etc/systemd/system/dockerregistry.service"

